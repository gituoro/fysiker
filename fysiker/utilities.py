import logging
from functools import wraps
import re
import datetime
import urllib.parse
import dateutil.tz


def parse_time(time):
    if time is None:
        return None
    if isinstance(time, str):
        time = datetime.datetime.strptime(time.strip(), '%H:%M').time()
    if time.tzinfo is None:
        time = time.replace(tzinfo=dateutil.tz.gettz())
    return time


def func_to_str(func, *args, **kwargs):
    arg_str = ', '.join([repr(v) for v in args] +
                        [f'k=repr(v)' for k, v in kwargs.items()])
    return f'{func.__name__}({arg_str})'


def log(level):
    def decorator(func):
        @wraps(func)
        def wrapped_func(self, *args, **kwargs):
            self.logger.log(level, f'{func_to_str(func, *args, **kwargs)}')
            ret = func(self, *args, **kwargs)
            # self.logger.log(level, f'{func_to_str(func)}')
            return ret
        return wrapped_func
    return decorator


log_info = log(logging.INFO)
log_debug = log(logging.DEBUG)


def clean_string(s):
    s = s.replace('\r\n', ' ')
    s = s.replace('\n', ' ')
    s = s.replace(',', ', ')
    while '  ' in s:
        s = s.replace('  ', ' ')
    s = s.strip()
    return s


def strptime(s, fmt):
    return datetime.datetime.strptime(s.strip(), fmt.strip())


def now():
    return datetime.datetime.now(tz=dateutil.tz.gettz())


def parse_booking_row(tr, page_url):
    from fysiker.activity import Activity

    th = tr.xpath('th')[0]
    datetime_string = clean_string(th.text_content()).split(' ', 1)[1]
    date_string = datetime_string.rsplit(' ', 3)[0]
    time_string = datetime_string.split(date_string, 1)[1]
    start_time, end_time = time_string.split('-')

    date = strptime(date_string, '%d %B').date()
    today = datetime.date.today()
    date = date.replace(year=today.year)
    # Around new year some of the bookings might be on next year
    if date < today:
        date = date.replace(year=today.year + 1)

    td = tr.xpath('td[@data-title="Activity"]')[0]
    title = clean_string(td.text_content())
    m = re.match(r'^(.*) \(reserv (\d+)\)$', title)
    if m is not None:
        title = m.group(1).strip()
        queue_number = int(m.group(2))
    else:
        queue_number = None
    td = tr.xpath('td[@data-title="Instructor"]')[0]
    instructor = clean_string(td.text_content())
    td = tr.xpath('td[@data-title="Location"]')[0]
    location = clean_string(td.text_content())
    m = re.match(r'(.*)\((.*)\)', location)
    locale = m.group(1).strip()
    room = m.group(2).strip()

    activity = Activity(date, start_time, end_time, title, instructor,
                        locale, room, queue_number=queue_number)

    td = tr.xpath('td[@data-title="Book"]')[0]
    action = clean_string(td.text_content())
    if len(td.xpath('a')) == 1:
        a = td.xpath('a')[0]
        url = urllib.parse.urljoin(page_url, a.get("href"))
        if 'Cancel' in action:
            activity.cancel_url = url
        else:
            RuntimeError(f'Unknown action: {action}')

    return activity


def parse_activity_row(li, page_url, date):
    from fysiker.activity import Activity

    div = li.xpath('div[@class="time"]')[0]
    time_string = clean_string(div.text_content())

    start_time, end_time = time_string.split('-')

    div = li.xpath('div[@class="title"]')[0]
    title = clean_string(div.text_content())
    div = li.xpath('div[@class="instructor"]')[0]
    instructor = clean_string(div.text_content())
    div = li.xpath('div[@class="location"]')[0]
    location = clean_string(div.text_content())
    m = re.match(r'(.*)\((.*)\)', location)
    room = m.group(1).strip()
    locale = m.group(2).strip()
    # div = li.xpath('div[@class="status"]')[0]
    # status = clean_string(div.text_content())

    activity = Activity(date, start_time, end_time, title, instructor,
                        locale, room)

    div = li.xpath('div[@class="button-holder"]')[0]
    action = clean_string(div.text_content())
    if len(div.xpath('a')) == 1:
        a = div.xpath('a')[0]
        url = urllib.parse.urljoin(page_url, a.get("href"))
        if 'Book' in action:
            activity.book_url = url
        elif 'Cancel' in action:
            activity.cancel_url = url
        else:
            RuntimeError(f'Unknown action: {action}')

    return activity
