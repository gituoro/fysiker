import os.path
import json
import argparse
import logging
import getpass
import random
import string
import threading
from fysiker.booker import Booker
from fysiker.scheduler import Scheduler, TimePeriod
from fysiker.reminder import Reminder, time_function
from fysiker.activity import ActivityTemplate
from fysiker.telegram_bot import TelegramBot


def setup(args):
    from telegram.ext import Updater
    from telegram.ext import CommandHandler

    config_fpath = args.config
    if os.path.exists(config_fpath):
        print(f'Configuration file {config_fpath} exists')
        print('Exiting')
        return

    # Configuration
    config = {}

    # Generate a key for verifying user
    chars = string.ascii_lowercase
    secret = ''.join(random.choices(chars, k=6))

    # Ask token
    token = getpass.getpass('Telegram token: ')
    config.update(telegram_bot_token=token)

    # Create a minimal telegram bot
    updater = Updater(token=token, use_context=True)
    print(f'Setting up {updater.bot.get_me()["username"]} for Fysiker')

    def stop():
        updater.is_idle = False
        updater.stop()

    def callback(update, context):
        msg = ''.join(context.args)
        if msg == secret:
            user = update.effective_user
            chat = update.effective_chat
            update.message.reply_text(f"Hello, {user.first_name}!")
            print(f'Bot linked with {user.first_name} ({user.id})')
            threading.Thread(target=stop).start()
            config.update(dict(telegram_user_id=user.id,
                               telegram_chat_id=chat.id))
        else:
            update.message.reply_text("Incorrect key")
            print(f'Please write "/{cmd} {secret}" to the bot')

    cmd = 'setup'
    updater.dispatcher.add_handler(CommandHandler(cmd, callback))
    print(f'Open a chat with the bot and write "/{cmd} {secret}"')

    updater.start_polling(clean=True, timeout=4)
    updater.idle()

    username = input('Fysiken username / email: ')
    config.update(fysiken_username=username)

    with open(config_fpath, 'w') as f:
        json.dump(config, f, sort_keys=True, indent=2)
    print(f'Configuration written to {config_fpath}')


def run(args):
    config_fpath = args.config
    if not os.path.exists(config_fpath):
        print(f'Configuration file {config_fpath} does not exist')
        print(f'Please run setup first')
        print('Exiting')
        return

    with open(config_fpath, 'r') as f:
        config = json.load(f)

    username = config.get('fysiken_username')
    password = config.get('fysiken_password', None)
    if password is None:
        password = getpass.getpass(f'Fysiken password for {username}: ')

    booker = Booker(username=username, password=password)
    bot = TelegramBot(booker=booker,
                      bookings_update_interval=1,
                      token=config['telegram_bot_token'],
                      user_id=config['telegram_user_id'],
                      chat_id=config['telegram_chat_id'])

    schedule_fpath = args.schedule
    if not os.path.exists(schedule_fpath):
        print(f'Schedule file {schedule_fpath} does not exist')
        print('Exiting')
        return

    with open(schedule_fpath, 'r') as f:
        schedule = json.load(f)

    # Reminder
    time_functions = []
    for name, arg in schedule['reminders']:
        f = time_function(name, arg)
        time_functions.append(f)
    reminder = Reminder(bot, time_functions)
    reminder.start()

    # Scheduler
    scheduled_activities = []
    for kwargs in schedule['activities']:
        a = ActivityTemplate(**kwargs)
        scheduled_activities.append(a)
    inactive_periods = []
    for kwargs in schedule['inactive_periods']:
        p = TimePeriod(**kwargs)
        inactive_periods.append(p)
    scheduler = Scheduler(bot, scheduled_activities, inactive_periods)
    scheduler.start()

    # Start
    bot.start()


def main():
    parser = argparse.ArgumentParser(description='Fysiker')
    parser.add_argument('--debug', action='store_true',
                        help='enable debugging output')
    subparsers = parser.add_subparsers(title='sub-commands')

    # Setup
    parser_setup = subparsers.add_parser('setup', help='setup fysiker')
    parser_setup.add_argument('config',
                              help='configuration file')
    parser_setup.set_defaults(func=setup)

    # Run
    parser_run = subparsers.add_parser('run', help='run fysiker')
    parser_run.add_argument('config',
                            help='configuration file')
    parser_run.add_argument('schedule',
                            help='schedule file')
    parser_run.set_defaults(func=run)
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.print_help()


if __name__ == '__main__':
    main()
