import logging
from datetime import datetime, timedelta

from fysiker.utilities import parse_time, now, log_debug


def time_function(name, arg):
    if name == 'before_start':
        time = parse_time(arg)
        return before_start(hours=time.hour,
                            minutes=time.minute)
    if name == 'previous_day':
        return previous_day(arg)
    if name == 'same_day':
        return same_day(arg)


def before_start(**kwargs):
    def f(dt):
        return dt - timedelta(**kwargs)
    return f


def previous_day(time):
    def f(dt):
        return datetime.combine(dt.date() - timedelta(days=1),
                                parse_time(time))
    return f


def same_day(time):
    def f(dt):
        return datetime.combine(dt.date(), parse_time(time))
    return f


class ActivityReminder:
    def __init__(self, bot, activity, time_functions):
        self._bot = bot
        self.activity = activity
        remind_dt_i = []
        for func in time_functions:
            remind_dt = func(self.activity.start_datetime)
            remind_dt_i.append(remind_dt)
        self.remind_dt_i = sorted(remind_dt_i)
        name = f'{__name__}.{self.__class__.__name__}'
        self.logger = logging.getLogger(name)

    @log_debug
    def remind_and_schedule_next(self):
        # Stop if activity isn't in bookings anymore
        if self.activity not in self._bot.bookings:
            return

        text = f'Reminder: {self.activity}'
        self._bot.show_message(text)
        self.schedule_next()

    @log_debug
    def schedule_next(self):
        # Stop if remind times are gone
        if len(self.remind_dt_i) == 0:
            return

        run_dt = self.remind_dt_i.pop(0)
        self.logger.info(f'reminding on {self.activity} on {run_dt}')
        self._bot.run_once(self.remind_and_schedule_next, run_dt)


class Reminder:
    def __init__(self, bot, time_functions):
        self._bot = bot
        self._reminders = {}
        self._time_functions = time_functions
        name = f'{__name__}.{self.__class__.__name__}'
        self.logger = logging.getLogger(name)

    def start(self):
        self.check_reminders()

    @log_debug
    def check_reminders(self):
        self.logger.info(f'checking reminders')

        # Add reminders for new bookings
        for activity in self._bot.bookings:
            if activity not in self._reminders:
                r = ActivityReminder(self._bot, activity, self._time_functions)
                r.schedule_next()
                self._reminders[activity] = r

        # Prune past activities
        for activity in self._reminders.keys():
            if activity.start_datetime < now():
                del self._reminders[activity]

        # Check later again
        run_dt = now() + timedelta(hours=12)
        self._bot.run_once(self.check_reminders, run_dt)
