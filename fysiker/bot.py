import logging
from datetime import timedelta

from fysiker.utilities import now, log_info, log_debug


class ActivityIterator:
    def __init__(self, booker, dt):
        self._booker = booker
        week_activities = self._fetch_week_activities(dt)
        activities = []
        for activity in week_activities:
            if dt < activity.start_datetime:
                activities.append(activity)
        self._dt = dt
        self._activities = activities

    def _fetch_week_activities(self, dt):
        year, week = [int(v) for v in dt.strftime('%G %V').split()]
        return self._booker.fetch_activities(year, week)

    def __iter__(self):
        return self

    def __next__(self):
        if len(self._activities) == 0:
            self._dt += timedelta(days=7)
            # Stop iteration if requesting time far in future
            # to avoid infite loop
            if self._dt > now() + timedelta(days=20):
                raise StopIteration
            self._activities += self._fetch_week_activities(self._dt)
        activity = self._activities.pop(0)
        return activity


class Bot:
    def __init__(self, booker, bookings_update_interval):
        self._booker = booker
        self._bookings = None
        if isinstance(bookings_update_interval, timedelta):
            self._bookings_update_interval = bookings_update_interval
        else:
            self._bookings_update_interval = \
                    timedelta(minutes=bookings_update_interval)
        name = f'{__name__}.{self.__class__.__name__}'
        self.logger = logging.getLogger(name)

    def show_message(self, text):
        return NotImplemented

    def show_bookings(self, text):
        return NotImplemented

    def run_once(self, func, when, *args):
        return NotImplemented

    def start(self):
        return NotImplemented

    @property
    @log_info
    def bookings(self):
        if (self._bookings is not None and self._bookings_refresh_dt > now()):
            self.logger.info('bookings from buffer')
            return self._bookings
        self._booker.login()
        self._bookings_refresh_dt = now() + self._bookings_update_interval
        self._bookings = self._booker.fetch_bookings()
        self._booker.logout()
        return self._bookings

    @log_debug
    def get_activity_iterator(self, dt):
        return ActivityIterator(self._booker, dt)

    @log_info
    def book(self, activity):
        self._booker.login()
        success = self._booker.book(activity)
        self.logger.info(f'success={success}')
        if success:
            self._bookings_refresh_dt = now()
        self._booker.logout()
        return success
