import json
import time
import logging
from fysiker.booker import Booker
from fysiker.scheduler import Scheduler, TimePeriod
from fysiker.reminder import Reminder
from fysiker.reminder import before_start, same_day, previous_day
from fysiker.activity import ActivityTemplate


with open('config.json', 'r') as f:
    config = json.load(f)


def test_activities():
    booker = Booker(username=config['username'], password=config['password'])
    activities = booker.fetch_activities(2020, 4)
    for act in activities:
        print(act)
        print(act._book_url)


def test_bookings():
    booker = Booker(username=config['username'], password=config['password'])
    booker.login()
    activities = booker.fetch_bookings()
    booker.logout()
    for act in activities:
        print(act)

    # This should be empty as we have logged out
    bookings = booker.fetch_bookings()
    print(bookings)


def test_book():
    from fysiker.activity import ActivityTemplate

    with open('schedule.json', 'r') as f:
        schedule = json.load(f)

    target_activity = ActivityTemplate(**schedule['activities'][-1])
    print('Target:', target_activity)

    booker = Booker(username=config['username'], password=config['password'])
    activities = booker.fetch_activities(2020, 4)
    # Find matching activity
    for activity in activities:
        if target_activity.matches(activity):
            a = activity
            break
    print('Found:', a)

    booker.login()
    if 1:
        print('Book', a)
        success = booker.book(a)
        a = None
        print(f'success {success}')
        time.sleep(5)
        activities = booker.fetch_bookings()
        print('Bookings:')
        for activity in activities:
            print(activity)
    if 1:
        activities = booker.fetch_bookings()
        print('Bookings:')
        for activity in activities:
            print(activity)
            if target_activity.matches(activity):
                a = activity
        print('Cancel', a)
        print(a.cancel_url)
        success = booker.cancel(a)
        a = None
        print(f'success {success}')
        time.sleep(5)
        print('Bookings:')
        for activity in activities:
            print(activity)
    booker.logout()


def hello(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text='Hello')


def test_bot():
    from fysiker.telegram_bot import TelegramBot

    logging.basicConfig(level=logging.INFO)

    booker = Booker(username=config['username'], password=config['password'])
    bot = TelegramBot(booker=booker,
                      bookings_update_interval=1,
                      token=config['telegram_bot_token'],
                      user_id=config['telegram_user_id'],
                      chat_id=config['telegram_chat_id'])
    bot.add_command('hello', hello)

    with open('schedule.json', 'r') as f:
        schedule = json.load(f)

    # Reminder
    time_functions = [previous_day('21:00'),
                      same_day('7:00'),
                      before_start(hours=3),
                      before_start(hours=1),
                      before_start(minutes=30),
                      ]
    reminder = Reminder(bot, time_functions)
    reminder.start()

    # Scheduler
    scheduled_activities = [ActivityTemplate(**a)
                            for a in schedule['activities']]
    # scheduled_activities = []
    inactive_periods = [TimePeriod(**p)
                        for p in schedule['inactive_periods']]
    # inactive_periods = []
    scheduler = Scheduler(bot, scheduled_activities, inactive_periods)
    scheduler.start()

    bot.start()


if __name__ == '__main__':
    # test_activities()
    # test_bookings()
    # test_book()
    test_bot()
