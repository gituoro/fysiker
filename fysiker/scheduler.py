import logging
from datetime import timedelta, datetime
from dateutil.tz import gettz

from fysiker.utilities import now, log_info, log_debug


def parse_datetime(dt):
    if isinstance(dt, str):
        dt = datetime.strptime(dt.strip(), '%Y-%m-%dT%H:%M')
    if dt.tzinfo is None:
        dt = dt.replace(tzinfo=gettz())
    return dt


class TimePeriod:
    def __init__(self, start, end):
        self.start = parse_datetime(start)
        self.end = parse_datetime(end)

    def __contains__(self, dt):
        if isinstance(dt, datetime):
            return self.start <= dt and dt < self.end
        return False


class Scheduler:
    def __init__(self, bot, scheduled_activities, inactive_periods):
        self._bot = bot
        self._scheduled_activities = scheduled_activities
        self._inactive_periods = inactive_periods
        self._activity_iterator = self._bot.get_activity_iterator(now())
        name = f'{__name__}.{self.__class__.__name__}'
        self.logger = logging.getLogger(name)

    def start(self):
        self.schedule_next()

    @log_debug
    def book_and_schedule_next(self, activity):
        self._bot.book(activity)
        self.schedule_next()

    @log_info
    def schedule_next(self):
        bookings = self._bot.bookings
        for activity in self._activity_iterator:
            self.logger.debug(activity)
            if not any(sched_activity.matches(activity)
                       for sched_activity in self._scheduled_activities):
                continue
            self.logger.info(f'match: {activity}')
            if activity.start_datetime < now():
                self.logger.info('skipping: past activity')
                continue
            if activity in bookings:
                self.logger.info('skipping: already booked')
                continue
            if any(activity.start_datetime in inactive_period
                   for inactive_period in self._inactive_periods):
                self.logger.info('skipping: during inactivity period')
                continue
            # Schedule booking
            run_dt = activity.start_datetime - timedelta(days=8, hours=-1,
                                                         minutes=-8)
            self.logger.info(f'scheduling to book on {run_dt}')
            self._bot.run_once(self.book_and_schedule_next, run_dt, activity)
            return
        self.logger.info('no more events')
        self._bot.show_message('No activities matching the schedule found')
