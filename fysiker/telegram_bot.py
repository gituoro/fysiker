from functools import wraps
from datetime import datetime
from dateutil.tz import gettz

import telegram
from telegram.ext import Updater
from telegram.ext import CommandHandler

from fysiker.bot import Bot
from fysiker.utilities import log_debug


def wrap_context_args(func):
    @wraps(func)
    def wrapped_func(context):
        return func(*context.job.context)
    return wrapped_func


class TelegramBot(Bot):
    def __init__(self, booker, bookings_update_interval,
                 token, user_id, chat_id):
        super().__init__(booker, bookings_update_interval)
        self._user_id = int(user_id)
        self._chat_id = int(chat_id)
        self._updater = Updater(token=token, use_context=True)
        self.add_command('bookings', self.show_bookings)

    @log_debug
    def add_command(self, command, func):
        h = CommandHandler(command, self._wrap_command_func(func))
        self._updater.dispatcher.add_handler(h)

    def _wrap_command_func(self, func):
        @wraps(func)
        def wrapped_func(update, context):
            user = update.effective_user
            if user.id != self._user_id:
                name = f'{user.first_name} {user.last_name} ({user.id})'
                print(f'Denied user: {name}')
                return
            chat_id = update.effective_message.chat_id
            context.bot.send_chat_action(chat_id=chat_id,
                                         action=telegram.ChatAction.TYPING)
            return func(update, context)
        return wrapped_func

    @log_debug
    def show_message(self, text):
        self._updater.bot.send_message(chat_id=self._chat_id, text=text)

    @log_debug
    def show_bookings(self, update, context):
        bookings = self.bookings
        if len(bookings) == 0:
            text = 'No bookings'
        else:
            text = '\n'.join([str(a) for a in bookings])
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)

    @log_debug
    def run_once(self, func, when, *args):
        # If 'when' is a naive datetime object,
        # give it a local time zone (opposed to UTC default)
        if isinstance(when, datetime) and when.tzinfo is None:
            when = when.replace(tzinfo=gettz())

        self._updater.job_queue.run_once(wrap_context_args(func), when,
                                         context=args)

    @log_debug
    def start(self):
        self._updater.start_polling(clean=True)

    @log_debug
    def book(self, activity):
        success = super().book(activity)
        if success:
            self.show_message('Booked ' + str(activity))
        else:
            self.show_message('Booking failed for ' + str(activity))
