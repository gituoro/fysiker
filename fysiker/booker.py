import logging
import http.cookiejar
import urllib.request
import urllib.parse
import lxml.html

from fysiker import __version__
from fysiker.utilities import parse_booking_row, parse_activity_row
from fysiker.utilities import strptime, clean_string, log_info


DOMAIN = 'https://fysiken.nu'


class Booker:
    def __init__(self, username, password):
        name = f'{__name__}.{self.__class__.__name__}'
        self.logger = logging.getLogger(name)
        cookiejar = http.cookiejar.CookieJar()
        handler = urllib.request.HTTPCookieProcessor(cookiejar)
        opener = urllib.request.build_opener(handler)
        opener.addheaders = [('User-Agent', f'Fysiker/{__version__}')]
        self._opener = opener
        self._cookiejar = cookiejar
        self._umbraco = f'{DOMAIN}/umbraco/Surface'
        self._username = username
        self._password = password

    @log_info
    def login(self):
        # Get login page
        url = f'{self._umbraco}/User/UserLogin'
        with self._opener.open(url) as f:
            data = f.read()
        tree = lxml.html.fromstring(data)

        # Find __RequestVerificationToken and other parameters from the form
        input_i = tree.xpath("//form//input")
        params = {}
        for inp in input_i:
            if inp.name not in ["Username", "Password", None]:
                params[inp.name] = inp.value
        params.update({"Username": self._username,
                       "Password": self._password})
        data = urllib.parse.urlencode(params).encode('ascii')

        # Post username and password
        self._opener.open(url, data=data)

    @log_info
    def fetch_bookings(self):
        # Get bookings page
        url = f'{DOMAIN}/en/my-account/my-bookings'
        with self._opener.open(url) as f:
            data = f.read()

        # Find table and parse bookings
        bookings = []
        tree = lxml.html.fromstring(data)
        table = tree.xpath("//table")[0]
        for tr in table.xpath("tbody/tr"):
            bookings.append(parse_booking_row(tr, url))
        return bookings

    @log_info
    def logout(self):
        # Get logout page
        url = f'{self._umbraco}/User/UserLogout?referrer=2228'
        self._opener.open(url)

    @log_info
    def fetch_activities(self, year, week):
        # Get booking page
        url = f'{DOMAIN}/en/book'
        with self._opener.open(url) as f:
            data = f.read()

        # Find list of facilities
        tree = lxml.html.fromstring(data)
        option_i = tree.xpath('//select[@id="unit"]/option')
        unit_i = []
        for opt in option_i:
            unit = opt.get('value')
            if unit != 'null':
                unit_i.append(unit)

        # Get activities page
        url = f'{self._umbraco}/ActivitySchedule/SearchActivitiesByWeekAndYear'
        params = [('year', year), ('week', week), ('locale', 'en-GB')]
        for unit in unit_i:
            params.append(('units', unit))
        data = urllib.parse.urlencode(params).encode('ascii')
        with self._opener.open(url, data=data) as f:
            data = f.read()

        # Parse activities
        activities = []
        tree = lxml.html.fromstring(data)
        day_li_i = tree.xpath('//li[contains(@class, "day")]')
        for day_li in day_li_i:
            weekday = day_li.get('data-title')
            date = strptime(f'{year} {week} {weekday}', '%G %V %A').date()
            act_li_i = day_li.xpath('.//li[contains(@class, "activity")]')
            for act_li in act_li_i:
                if 'header' in act_li.get('class'):
                    continue
                activities.append(parse_activity_row(act_li, url, date))
        return activities

    def __book_or_cancel(self, activity, book=True):
        # If activity doesn't have book/cancel url, get up-to-date activities
        # and pick the equivalent activity with an url
        if not activity.has_url:
            dt = activity.date
            year, week = [int(v) for v in dt.strftime('%G %V').split()]
            activities = self.fetch_activities(year, week)
            activity = next(a for a in activities if a == activity)

        # Get book/cancel page
        if book:
            url = activity.book_url
        else:
            url = activity.cancel_url
        if url is None:
            return False

        with self._opener.open(url) as f:
            data = f.read()

        tree = lxml.html.fromstring(data)
        form = tree.xpath("//form")[0]
        url = f'{DOMAIN}{form.get("action")}'
        input_i = form.xpath("input")
        params = {}
        for inp in input_i:
            params[inp.name] = inp.value
        data = urllib.parse.urlencode(params).encode('ascii')

        # Post booking
        if '?' not in url:
            url += '?locale=en-GB'
        with self._opener.open(url, data=data) as f:
            data = f.read()

        success = False
        msg = clean_string(lxml.html.fromstring(data).text_content())
        if book:
            success = 'Booking completed!' in msg
            if success:
                activity.book_url = None
        else:
            success = 'Your reservation has been removed.' in msg
            if success:
                activity.cancel_url = None

        return success

    @log_info
    def book(self, activity):
        return self.__book_or_cancel(activity, book=True)

    @log_info
    def cancel(self, activity):
        return self.__book_or_cancel(activity, book=False)
