import datetime
from fysiker.utilities import parse_time


class ActivityTemplate:
    def __init__(self, weekday=None, start_time=None, end_time=None,
                 title=None, instructor=None, locale=None, room=None):
        self.weekday = weekday
        self.start_time = parse_time(start_time)
        self.end_time = parse_time(end_time)
        self.title = title
        self.instructor = instructor
        self.locale = locale
        self.room = room

    def matches(self, activity, skip_none=True, exact_match=False,
                case_sensitive=False):
        for attr in ['weekday', 'start_time', 'end_time', 'title',
                     'instructor', 'locale', 'room']:
            self_attr = getattr(self, attr)
            if skip_none and self_attr is None:
                continue
            other_attr = getattr(activity, attr)
            if not case_sensitive and isinstance(self_attr, str):
                self_attr = self_attr.lower()
                other_attr = other_attr.lower()
            if exact_match or not isinstance(self_attr, str):
                if self_attr != other_attr:
                    return False
            else:
                if self_attr not in other_attr:
                    return False
        return True

    def __hash__(self):
        return hash((self.weekday, self.start_time, self.end_time,
                     self.title, self.instructor, self.locale, self.room))

    def __repr__(self):
        time_string = self.start_time.strftime('%H:%M')
        dt_string = f'{self.weekday} {time_string}'
        return (f'{self.__class__.__name__}('
                f'{dt_string}: {self.title}, {self.locale}'
                f')')


class Activity(ActivityTemplate):
    def __init__(self, date, start_time, end_time, title, instructor,
                 locale, room, queue_number=None):
        weekday = date.strftime('%a')
        super().__init__(weekday, start_time, end_time, title, instructor,
                         locale, room)
        self.date = date
        self.queue_number = queue_number
        self._book_url = None
        self._cancel_url = None

    def __eq__(self, other):
        if not isinstance(other, Activity):
            return False
        if self.date != other.date:
            return False
        return self.matches(other, skip_none=False, exact_match=True,
                            case_sensitive=True)

    def __hash__(self):
        return hash((super().__hash__(), self.date))

    def __repr__(self):
        dt_str = self.start_datetime.strftime('%a %b %d %H:%M')
        queue_str = ''
        if self.queue_number is not None:
            queue_str = f' (queue {self.queue_number})'
        return (f'{self.__class__.__name__}('
                f'{dt_str}{queue_str}: {self.title}, {self.locale}'
                f')')

    @property
    def start_datetime(self):
        return datetime.datetime.combine(self.date, self.start_time)

    @property
    def end_datetime(self):
        return datetime.datetime.combine(self.date, self.end_time)

    @property
    def book_url(self):
        return self._book_url

    @book_url.setter
    def book_url(self, url):
        self._book_url = url

    @property
    def cancel_url(self):
        return self._cancel_url

    @cancel_url.setter
    def cancel_url(self, url):
        self._cancel_url = url

    @property
    def has_url(self):
        return self.book_url is not None or self.cancel_url is not None
