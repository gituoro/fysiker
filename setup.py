import re
import setuptools
import pathlib

with open("README.md", "r") as fh:
    long_description = fh.read()

txt = pathlib.Path("fysiker/__init__.py").read_text()
version = re.search("__version__ = '(.*)'", txt).group(1)

classifiers = [
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    "Operating System :: OS Independent"]

setuptools.setup(
    name="fysiker",
    version=version,
    author="Tuomas Rossi",
    author_email="gituoro@gmail.com",
    description="Fysiker",
    license="GPLv3",
    platforms=["Linux"],
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/gituoro/fysiker",
    packages=setuptools.find_packages(),
    entry_points={'console_scripts': ['fysiker=fysiker.main:main']},
    classifiers=classifiers,
    python_requires='>=3.6',
    )
