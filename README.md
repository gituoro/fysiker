# Fysiker

Python-based tools and bot for
booking activities at [Fysiken](https://fysiken.nu).


## Getting started

1. Create a telegram bot by
   [talking to BotFather](https://core.telegram.org/bots#6-botfather)
   or use an existing bot.
   Take a note on the `token`.

1. Set up paths or install:
   ```bash
   python3 setup.py install
   ```

1. Create a configuration file:
   ```bash
   fysiker setup config.json
   ```

1. Set up a schedule file.
   Use [example schedule](example-schedule.json)
   as a starting point.

1. Run the bot:
   ```bash
   fysiker run config.json schedule.json
   ```

